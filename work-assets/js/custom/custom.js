(function ($) {
  $(document).ready(function () {
    // Header Search
    if ($(".h__search").length) {
      var mainNav = $(".h__main__nav");
      var searchContainer = $(".h__search__content");

      $(".toggleSearch").on("click", function (e) {
        e.preventDefault();
        $(this).addClass("visibleHidden");
        mainNav.addClass("visibleHidden");
        searchContainer.removeClass("visible-hidden").addClass("showHidden");
        setTimeout(function () {
          $(".h__search__input").focus();
        }, 300);
      });

      $(".toggleDefault").on("click", function (e) {
        e.preventDefault();
        $(".toggleSearch").removeClass("visibleHidden");
        mainNav.removeClass("visibleHidden");
        searchContainer.removeClass("showHidden").addClass("visible-hidden");
      });

      // Dynamic padding for search dropdown
      var searchPadding = function () {
        var leftOffset = $(".container").offset().left + 8;
        searchContainer.css({
          "padding-right": leftOffset
        });
      };
      searchPadding();
      window.addEventListener("resize", searchPadding);
    }

    // Custom Select Dropdown
    if ($(".vin__select").length) {
      if ($(".vin__select__dropdown li a").hasClass("active")) {
        var $this = $(".vin__select__dropdown li a.active");

        var linkTest = $this.text(),
          selectedText = $this
            .parents(".vin__select")
            .find(".vin__select__toggle span");

        selectedText.addClass("selectedOption");
        selectedText.text(linkTest);
      }

      $(".vin__select__toggle").on("click", function (e) {
        e.preventDefault();

        var selectParent = $(this),
          selectDropdown = $(".vin__select__dropdown"),
          selectLinks = $(".vin__select__dropdown li"),
          selectActiveClass = "active";

        selectParent.toggleClass(selectActiveClass);
        selectParent.next().slideToggle("fast");

        selectLinks.each(function () {
          var selctLink = $(this).find("a");

          selctLink.on("click", function (e) {
            e.preventDefault();

            var linkTest = $(this).text(),
              selectedText = $(this)
                .parents(".vin__select")
                .find(".vin__select__toggle span");

            $(".vin__select__dropdown li a").removeClass(selectActiveClass);
            $(this).addClass(selectActiveClass);
            selectedText.addClass("selectedOption");
            selectedText.text(linkTest);
            selectDropdown.slideUp("fast");
            selectParent.removeClass(selectActiveClass);
          });
        });
      });

      $(document).click(function (event) {
        if (
          !$(event.target).closest(".vin__select ").length &&
          !$(event.target).closest(".vin__select__dropdown").length
        ) {
          if ($(".vin__select__dropdown").is(":visible")) {
            $(".vin__select__dropdown").slideUp("fast");
            $(".vin__select__toggle").removeClass("active");
          }
        }
      });
    }

    // Main Navigation
    if ($(".h__main__nav").length) {
      function addArrow() {
        var dropLinks = $(".h__main__nav li.has-dropdown");
        var arrow =
          '<span class="toggleAccordion"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 7.85 3.92" fill="#fff"><path d="M1.42 0H0l3.92 3.92L7.85 0H6.43L3.92 2.51 1.42 0z"/></svg></span>';

        if ($(window).innerWidth() <= 800) {
          dropLinks.each(function () {
            if ($(this).find("svg").length < 1) {
              $(this).prepend(arrow);
            }
          });
        } else {
          $(".h__main__nav li.has-dropdown .toggleAccordion").remove();
        }
      }
      addArrow();

      window.addEventListener("resize", addArrow);

      // Toggle Nav Accordion
      $(document).on("click", ".toggleAccordion", function (e) {
        e.preventDefault();
        $(this)
          .find("svg")
          .toggleClass("openDropdown");
        $(this)
          .closest("li.has-dropdown")
          .find(".h__main__nav__dropdown")
          .slideToggle("fas");
      });

      // Open Sidenav
      if ($(".toggleNav").length) {
        $(document).on("click", ".toggleNav", function (e) {
          e.preventDefault();
          if ($('.h__search__content').hasClass('showHidden')) {
            $('.h__search__content').removeClass('showHidden').addClass('visible-hidden');
          }

          if ($('.h__main__nav').hasClass('visibleHidden')) {
            $('.h__main__nav').removeClass('visibleHidden')
          }

          $(this)
            .closest(".vin__hamburger")
            .toggleClass("active");
          $(this)
            .closest(".h__main__pri")
            .find(".h__main__nav")
            .toggleClass("openNav");
          $(".vin__overlay").toggleClass("navOpened");
          $('.h__search__icon').toggleClass('visibleHidden');
          $('.js-where-to-mobile-link').toggleClass('visibleHidden')
        });

        $(".vin__overlay").on("click", function () {
          $(this).removeClass("navOpened");
          $(".vin__hamburger").removeClass("active");
          $(".h__main__nav").removeClass("openNav");
          $('.h__search__icon').removeClass('visibleHidden');
          $('.js-where-to-mobile-link').removeClass('visibleHidden');
        });
      }

      // Remove unwanted classes if the device is width is greater 800px
      function removeUnwantedClasses() {
        if ($(window).innerWidth() >= 801) {
          if ($(".vin__hamburger").hasClass("active")) {
            $(".vin__hamburger").removeClass("active");
          }

          if ($(".h__main__nav").hasClass("openNav")) {
            $(".h__main__nav").removeClass("openNav");
          }

          if ($(".vin__overlay").hasClass("navOpened")) {
            $(".vin__overlay").removeClass("navOpened");
          }

          if($('.h__search__icon').hasClass('visibleeHidden')) {
            $('.h__search__icon').removeClass('visibleeHidden');
          }

          if($('.js-where-to-mobile-link').hasClass('visibleHidden')) {
            $('.js-where-to-mobile-link').removeClass('visibleHidden');
          }
        }
      }

      removeUnwantedClasses();

      window.addEventListener("resize", removeUnwantedClasses);
    }

    // Add href to marker icon
    if($('.js-where-to-mobile-link').length) {
      var url = $('.js-where-to-link').find('a').attr('href');
      $('.js-where-to-mobile-link').attr('href', url);
    }

    // Search Scripts
    if ($('.sec__result__wrapper').length) {
      // Init Scroll bar for the result list
      var swiper = new Swiper('.swiper-container', {
        direction: "vertical",
        slidesPerView: "auto",
        mousewheelControl: true,
        resistance: false,
        resistanceRatio: 0,
        freeMode: true,
        scrollbar: {
          el: '.swiper-scrollbar',
        },
        mousewheel: true
      });
    }

    // Generic Page Testimonial Slider
    if ($('.vin__testi').length) {
      $('.vin__testi__init').slick({
        prevArrow: $('.vin__testi__arrow--prev'),
        nextArrow: $('.vin__testi__arrow--next'),
        slideToShow: 1,
        slideToScroll: 1,
        dots: true,
        appendDots: $('.vin__testi__dots'),
        cssEase: 'cubic-bezier(0.77, 0, 0.175, 1)',
        speed: 1500,

        responsive: [
          {
            breakpoint: 801,
            settings: {
              arrows: false,
              centerMode: true,
              speed: 800,
              centerPadding: '44px'
            }
          }
        ]
      });
    }

    // Generic Page Gallery Slider
    if ($('.vin__gallery').length) {
      $('.vin__gallery__init').slick({
        prevArrow: $('.vin__gallery__arrow--prev'),
        nextArrow: $('.vin__gallery__arrow--next'),
        slideToShow: 1,
        slideToScroll: 1,
        cssEase: 'cubic-bezier(0.77, 0, 0.175, 1)',
        speed: 1500,

        responsive: [
          {
            breakpoint: 801,
            settings: {
              arrows: false,
              centerMode: true,
              speed: 800,
              centerPadding: '21.1%'
            }
          }
        ]
      });

      var subSlideItemsWrapper = $('.vin__gallery__sub .vin__gallery__items');
      subSlideItemsWrapper.each(function () {
        var subSlideItem = $(this).find('.vin__gallery__item');

        if (subSlideItem.length === 2) {
          $(this).addClass('twoRow');
        }
      });
    }

    // Vintec Custom Checkbox
    if ($('.vin__checkbox').length) {
      var vinCheckbox = $('.vin__checkbox');

      vinCheckbox.each(function () {
        var checkLabel = $(this).find('label');

        checkLabel.on('click', function (e) {
          e.preventDefault();
          var checkBox = $(this).prev('input[type="checkbox"]');
          checkBox.prop("checked", !checkBox.prop("checked"));

          $(this).closest('.vin__checkbox').toggleClass('checked');
        });
      });
    }

    // Vintec Custom Product Range Filters
    if ($('.vin__range__input').length) {
      $('.vin__range__input').each(function () {
        var rangeId = $(this).attr('id');

        $('#' + rangeId).slider({
          formatter: function formatter(val) {
            if (Array.isArray(val)) {
              return val[0] + " - " + val[1];
            } else {
              return val;
            }
          }
        });
      });
    }

    // Vintec Products Scripts
    if ($('.pro__wrapper').length) {
      // Toggle Filters
      function filterState() {
        if ($(window).innerWidth() <= 800) {
          $('.js-toggleFilters').closest('.p-filters__wrapper').find('.p-filters__content').toggleClass('js-openFilters');
        } else {
          $('.js-toggleFilters').closest('.p-filters__wrapper').find('.p-filters__content').toggleClass('js-openFilters').slideToggle('slow');
        }
      }

      $('.js-toggleFilters').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('js-filtersToggled');
        filterState();
      });

      $('.js-closeFilters').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.p-filters__content').removeClass('js-openFilters');
        $(this).closest('.p-filters__wrapper').find('.js-toggleFilters').removeClass('js-filtersToggled');
      })

      // Comparing Scripts
      var productList = [];
      var comparePanel = $('.pro__selected');

      $(document).on('click', '.js-productCompare', function (e) {
        e.preventDefault();
        var parentItem = $(this).closest('.p-list__item');

        $(this).toggleClass('selected');
        parentItem.toggleClass('selectedProduct');

        var selectedProductCode = parentItem.find('.p-list__code').data('product-code');
        var inArray = $.inArray(selectedProductCode, productList);

        if (comparePanel.hasClass('slideComparePanel')) {
          comparePanel.removeClass('slideComparePanel');
        }
        $('.p-selected__htext').removeClass('hidden');
        $('.p-selected__ctext').addClass('hidden');

        if (inArray < 0) {
          if (productList.length < 3) {
            productList.push(selectedProductCode);
            comparePanel.removeClass('hidden');

            var selectedProductSummary = parentItem.find('.p-list__summary').data('product-summary');
            var selectedProductImage = parentItem.find('.p-list__img__wrap img').attr('src');

            if (selectedProductCode && selectedProductSummary) {
              var selectedTemplate = '<div id="' + selectedProductCode + '" class="p-selected__item">' +
                '<div class="p-selected__content productPresent">' +
                '<a href="#" class="p-selected__remove js-removeSelected"><span>X</span></a>' +
                '<div class="p-selected__info">' +
                '<img class="mobile p-selected__img" src="' + selectedProductImage + '">' +
                '<h4 class="desktop p-list__code">' + selectedProductCode + '</h4>' +
                '<svg class="desktop" xmlns="http://www.w3.org/2000/svg">' +
                '<line x1="0" x2="100%" y1="5" y2="5" stroke="#fff" stroke-width="1.5" stroke-linecap="round" stroke-dasharray="1, 8"></line>' +
                '</svg>' +
                '<h5 class="c-white p-list__summary">' + selectedProductSummary + '</h5>' +
                '</div>' +
                '</div>' +
                '</div>';
            }

            $('.p-selected__items__wrapper').append(selectedTemplate);
          }
        } else {
          productList.splice($.inArray(selectedProductCode, productList), 1);
          var product = selectedProductCode.replace(" ", "");
          $('#' + product).remove();
          hideComparePanel();
        }

        if (productList.length > 1) {
          $('#initCompare').prop('disabled', false);
        } else {
          $('#initCompare').prop('disabled', true);
        }

        if ($('.p-list__item.selectedProduct').length === 3) {
          if ($('.p-list__item:not(.selectedProduct)')) {
            $('.p-list__item:not(.selectedProduct)').addClass('disable');
          }
        } else if ($('.p-list__item.selectedProduct').length < 3) {
          if ($('.p-list__item').hasClass('disable')) {
            $('.p-list__item').removeClass('disable');
          }
        }
      });

      $(document).on('click', '.js-removeSelected', function (e) {
        e.preventDefault();
        var productCode = $(this).closest('.p-selected__item').attr('id');
        $('[data-product-code=' + productCode + ']').find(".js-productCompare").click();
        hideComparePanel();
      });

      $('#cancelCompare').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.pro__selected').addClass('hidden');
        $(this).closest('.pro__selected').find('.p-selected__item').remove();
        $('#initCompare').prop('disabled', true);
        $('.p-list__item').removeClass('selectedProduct disable');
        $('.js-productCompare').removeClass('selected');
        productList = [];
      });

      $('.toggleComparePanel').on('click', function (e) {
        e.preventDefault();
        $('.pro__selected').toggleClass('slideComparePanel');
        $(this).find('.p-selected__htext').toggleClass('hidden');
        $(this).find('.p-selected__ctext').toggleClass('hidden');

        var selectedItem = $('.p-selected__item').length;
        $('.p-selected__num').text(selectedItem);
      });

      function moveCompareBtn() {
        var parentItem = $('.p-list__item');
        parentItem.each(function () {
          var mainCompBtnParent = $(this).find('.p-list__img__overlay');
          var mainCompBtn = mainCompBtnParent.find('.js-productCompare');
          var secCompBtnParent = $(this).find('.p-list__qsum__btn');

          if ($(window).innerWidth() <= 800) {
            mainCompBtn.clone(true).appendTo(secCompBtnParent);
            mainCompBtn.remove();
          } else {
            secCompBtnParent.find('.js-productCompare').clone(true).appendTo(mainCompBtnParent);
            secCompBtnParent.find('.js-productCompare').remove();
          }
        });
      }

      moveCompareBtn();
      window.addEventListener('resize', moveCompareBtn);

      // var senseSpeed = 5;
      // var previousScroll = 0;
      // $(window).scroll(function(event){
      //   var scroller = $(this).scrollTop();
      //   if (scroller-senseSpeed > previousScroll){
      //   $('.pro__selected').removeClass('slideComparePanel');
      //   $('.p-selected__htext').addClass('hidden');
      //   $('.p-selected__ctext').removeClass('hidden');

      //   var selectedItem = $('.p-selected__item').length;
      //   $('.p-selected__num').text(selectedItem);
      //   }
      //   previousScroll = scroller;
      // });

      function hideComparePanel() {
        if (!productList.length) {
          $(".p-selected__items__wrapper").empty();
          $(".pro__selected").addClass('hidden');
          $(".p-selected__ctext").addClass('hidden');
          if ($(".p-selected__htext").hasClass('hidden')) {
            $(".p-selected__htext").removeClass('hidden');
          }
          $('.p-selected__num').text(' ');
        }
      }
    }

    // Vintec Product Detail Hero Slider
    if ($('.p-detail__slider').length) {
      $('.p-detail__shinit').slick({
        prevArrow: $('.p-shmain__arrow--prev'),
        nextArrow: $('.p-shmain__arrow--next'),
        slidesToShow: 1,
        slideToScroll: 1,
        cssEase: 'cubic-bezier(0.77, 0, 0.175, 1)',
        speed: 1500,
        asNavFor: '.p-detail__svinit'
      });

      $('.p-detail__svinit')
        .on('init', function (slick) {
          if ($('.p-detail__svitem').length <= 5) {
            $('.p-detail__svinit .slick-track').addClass('noTranslate');
          }
        })
        .slick({
          prevArrow: $('.p-svsec__arrow--prev'),
          nextArrow: $('.p-svsec__arrow--next'),
          slidesToShow: 5,
          slideToScroll: 1,
          cssEase: 'cubic-bezier(0.77, 0, 0.175, 1)',
          speed: 1500,
          asNavFor: '.p-detail__shinit',
          vertical: true,
          focusOnSelect: true
        });
    }

    // Product Features Slider
    if ($('.p-features__swrapper').length) {
      $('.p-features__sinit').slick({
        prevArrow: $('.p-features__arrow--prev'),
        nextArrow: $('.p-features__arrow--next'),
        slideToShow: 1,
        slideToScroll: 1,
        dots: true,
        appendDots: $('.p-features__dots'),
        cssEase: 'cubic-bezier(0.77, 0, 0.175, 1)',
        speed: 1500,
      });
    }

    // Products Detail Scroll to div
    if ($('.p-detail__btns').length) {
      var offsetSecTop;
      var secId;

      function offsetDetailTop(secId) {
        if ($(window).innerWidth() <= 800) {
          offsetSecTop = $(secId).offset().top - 55
        } else {
          offsetSecTop = $(secId).offset().top
        }
      }

      $(window).resize(function () {
        offsetDetailTop(secId);
      });

      $(document).on('click', '.p-detail__btns a[href^="#"]', function (e) {
        e.preventDefault();
        secId = $(this).attr('href');

        offsetDetailTop(secId);

        $('html, body').stop().animate({
          scrollTop: offsetSecTop
        }, 500);
      });
    }

    // Compare Sticky Code
    if ($('.comp-pro__codes').length) {
      var s = $(".comp-pro__codes");
      var pos = s.position();
      $(window).scroll(function () {
        var windowpos = $(window).scrollTop();
        if (windowpos >= pos.top) {
          s.addClass("js-stick");
          $('.comp-pro__features').css({
            'padding-top': s.outerHeight(true)
          });
          s.css({
            'width': $('.compare__products').innerWidth()
          });
        } else {
          s.removeClass("js-stick");
          $('.comp-pro__features').css({
            'padding-top': 0
          });
          s.css({
            'width': 'auto'
          });
        }
      });
    }

    // Compare Scroll Indicator in mobile
    if ($('.compare__wrapper').length) {
      $('.compare__wrapper').on('scroll', function () {
        if ($(this).scrollLeft() > 0) {
          $('.compare__scrollindi').hide();
        } else {
          $('.compare__scrollindi').show();
        }
      });
    }

    // Custom tab
    if ($('.vin__tab').length) {
      $('.vin__tab__item a').on('click', function (e) {
        e.preventDefault();

        var $this = $(this),
          tabgroup = '#' + $this.parents('.vin__tab__items').data('tabgroup'),
          othersLinks = $this.closest('li').siblings().children('a'),
          target = $this.attr('href');

        if ($('.vin__tab__qinfo').length) {
          var qInfos = $('.vin__tab__qinfo');
          qInfos.removeClass('tabActive');
          $('.vin__tab__qinfo[data-quick-info=' + target.replace('#', '') + ']').addClass('tabActive');
        }

        othersLinks.removeClass('tabActive');
        $this.addClass('tabActive');
        $(tabgroup).children('.vin__tab__pane').removeClass('tabActive');
        $(target).addClass('tabActive');
      });
    }

    // Vintec ScrollTop
    if ($('.vin__scrolltop').length) {
      var scrollTopBtnWrap = $('.vin__scrolltop');

      $(window).scroll(function () {
        var topPos = $(this).scrollTop();

        if (topPos > 800) {
          $(scrollTopBtnWrap).addClass('show');
        } else {
          $(scrollTopBtnWrap).removeClass('show');
        }
      });

      $('.js-scrollTop').on('click', function (e) {
        e.preventDefault();

        $('html, body').stop().animate({
          scrollTop: 0
        }, 500);
      });
    }

    // Blog page Toggle Dropdowns in Mobile
    if ($('.blog__dropdowns').length) {
      $(document).on('click', '.js-toggleDropLink', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $(this).next('.b-dropdown__items').slideToggle('fast');
      });
    }

    // Vintec Accordion
    if ($('.vin__accordion').length) {
      var accordionTitle = $('.vin__accordion__title');

      accordionTitle.on('click', function (e) {
        e.preventDefault();
        var accordionContainer = $(this).closest('.vin__accordion__item');
        var accordionBody = accordionContainer.find('.vin__accordion__body');

        $(this).toggleClass('active');
        accordionBody.slideToggle(300);
      });
    }

    // Add Class to "li" with class "has-dropdown" to make it Keyboard Accessible
    if ($('li.has-dropdown').length) {
      $.fn.accessibleDropDown = function () {
        var el = $(this);

        /* Make dropdown menus keyboard accessible */
        $("a", el).focus(function () {
          $(this).parents("li.has-dropdown").addClass("hovered");
        }).blur(function () {
          $(this).parents("li.has-dropdown").removeClass("hovered");
        });
      }

      $('nav').accessibleDropDown();
    }
  });
})(jQuery);
