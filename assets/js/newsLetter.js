$(function() {

  var errorContainer = $('#js-formErrorMessages');

  $('.infusion-form').validate({
    rules: {
      inf_field_Email: {
        required: true,
        email: true
      }
    },

    errorLabelContainer: errorContainer,    
    messages: {
      inf_field_Email: 'Please enter valid email address!'      
    }
  });

  // Get the form.
  var formId = $('.sec__subscribe__form').attr('id');
  var form = $('#'+formId);
  
  // Get Message Div
  var formMessage = $('#js-formMessages');

  $(form).submit(function(e) {
    e.preventDefault();

    var action = $(form).attr('action')
    var formData = $(form).serialize();

    $.ajax({
      type: 'POST',
      url: action,
      data: formData,      
      success: function(data) {        
        console.log(data);        
      },
      error: function(data) {
        console.log(data);
      }
    });
  });
  
});